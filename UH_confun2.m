
function [c, ceq] = UH_confun2(x, SumMark)
% Nonlinear inequality constraints

[~,idx] = max(x);
c = [x(1:idx-1)-x(2:idx); x(idx+1:end)-x(idx:end-1); 0-x];

% Nonlinear equality constraints
if SumMark==1
    ceq = sum(x)-1;
else
    ceq = 0;
end